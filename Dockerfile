FROM ubuntu:18.04

WORKDIR /app
EXPOSE 8000

#COPY gdb8.3.deb /app
ADD tools.tar.gz /mnt/toolchain

RUN apt-get update && \
    apt-get install -y python3 python3-pip flex bison zlib1g libtinfo5 libpython2.7 libpython3.6 libreadline7 libmpfr-dev libiberty-dev liblzma5 libexpat1 libc6 libbabeltrace1 libbabeltrace-ctf1 && \
    pip3 install gdbgui
#    dpkg -i /app/gdb8.3.deb

ENTRYPOINT gdbgui --port 8000 --host 0.0.0.0 --no-browser --gdb /mnt/toolchain/tools/bin/mips64-octeon-linux-gnu-gdb
